import { createStore, applyMiddleware, StoreEnhancer } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

const middleware: StoreEnhancer = applyMiddleware(thunk);

export const store = createStore(reducers, {}, middleware);
